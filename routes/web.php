<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'jenis'], function () use ($router) {
    $router->get('/', ['uses' => 'JenisController@index']);
    $router->get('/get-data/{id}', ['uses' => 'JenisController@getData']);
    $router->post('/save', ['uses' => 'JenisController@save']);
    $router->get('update', ['uses' => 'JenisController@update']);
    $router->get('/delete/{id}', 'JenisController@delete');
});

$router->get('/get-user', ['uses' => 'AuthController@getUser']);

$router->group(['prefix' => 'auth'], function () use ($router) {
    $router->post('login',['uses' => 'AuthController@authenticate']);
    $router->post('register',['uses' => 'AuthController@register']);
});

$router->group(['middleware' => 'jwt.auth'], function() use ($router) {
    $router->get('users/{id}', ['uses' => 'AuthController@getUserJWT']);
    $router->get('my-profile/{id}', ['uses' => 'MembersController@myProfile']);
});

$router->group(['prefix' => 'api', 'middleware' => 'jwt.auth'], function () use ($router){
    $router->get('get-member/{id}', ['uses' => 'MembersController@getMember']);
    $router->put('member/update/{id}', ['uses' => 'MembersController@updateMember']);
});