<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Members extends Model {
    protected $table = 'members';
    protected $fillable = ['company_id','division_id','religion_id','country_id','nip','member_name',
                            'gender','email','phone','address','date_birth','date_join','photo_profile',
                            'status'];
    protected $hidden = [ 'id' ];
}