<?php

namespace App\Http\Controllers;

use App\Models\Members;
use Illuminate\Http\Request;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Firebase\JWT\JWT;
// use Illuminate\Http\Request;
use Firebase\JWT\ExpiredException;
use Illuminate\Support\Facades\Hash;
use App\User;

class MembersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getMember($id){
        $data = Members::where('id',$id)->first();
        $response['success'] = true;
        if($data){
            $response['message'] = 'Your Data';
            $response['data'] = $data;
        }else{
            $response['success'] = false;
            $response['message'] = 'Data Does not Exist';
            $response['data'] = '';
        }
        return response()->json($data);
    }

    public function updateMember(Request $request,$id){
        $members = Members::find($id);
        // dd($members);
        $file = $request->file('photo_profile');
    	// $file = $request->file('lampiran');
    	if ($file != null) {
	        $new_file = $file->getClientOriginalName();
	        $file->move(base_path('public/files'), $new_file);
	        $members->photo_profile = $new_file;  
	    }
        //$members->tgl_buat_members = $request->tgl_buat_members;
	    $members->company_id = $request->company_id;
	    $members->division_id = $request->division_id;
	    $members->religion_id = $request->religion_id;
	    $members->country_id = $request->country_id;
        $members->nip = $request->nip;
        $members->member_name = $request->member_name;
	    $members->gender = $request->gender;
	    $members->email = $request->email;
	    $members->phone = $request->phone;
        $members->address = $request->address;
        $members->date_birth = $request->date_birth;
	    $members->date_join = $request->date_join;
	    // $members->photo_profile = $request->photo_profile;
        $members->status = $request->status;

        $response['success'] = true;

        if ($members && $members->save()) {
            $response['status'] = 'success';
            $response['message'] = 'successfully updated';
            $response['data'] = $members;
        }else{
            $response['status'] = 'fail';
            $response['success'] = false;
            $response['message'] = 'failed to update';
            $response['data'] = '';
        }

        return response()->json($response);
    }

    public function myMember(Request $request,$id){
        $token = $request->get('token');
        $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);
        
        $user = User::where('email',$credentials->email);
        // return json_encode($credentials->id);
        // return $user->id;
        if($id != $credentials->id){
            return response()->json([
                'result' => false,
                'data' => 'not exist'
            ]);    
        }else{
            return response()->json([
                'result' => true,
                'data' => $user
            ]);
        }    
    }
}
