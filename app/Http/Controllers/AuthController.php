<?php
namespace App\Http\Controllers;
use Validator;
use App\User;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Firebase\JWT\ExpiredException;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Closure;
class AuthController extends BaseController 
{
    /**
     * The request instance.
     *
     * @var \Illuminate\Http\Request
     */
    private $request;
    /**
     * Create a new controller instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    public function __construct(Request $request) {
        $this->request = $request;
    }
    /**
     * Create a new token.
     * 
     * @param  \App\User   $user
     * @return string
     */
    protected function jwt(User $user) {
        $payload = [
            'iss' => "lumen-jwt", // Issuer of the token
            'sub' => $user->id, // Subject of the token
            'iat' => time(), // Time when JWT was issued. 
            'exp' => time() + 60*60 // Expiration time
        ];
        
        // As you can see we are passing `JWT_SECRET` as the second parameter that will 
        // be used to decode the token in the future.
        return JWT::encode($payload, env('JWT_SECRET'));
    }
    /**
     * Authenticate a user and return the token if the provided credentials are correct.
     * 
     * @param  \App\User   $user 
     * @return mixed
     */

    public function getUser(){
        $data = Auth::user()->id;
        return response()->json($data);
    }

    public function checkServer($email, $password){
        return file_get_contents('http://maillogin.ved.carsworld.id/?username='.$email.'&password='.$password);
    }

    public function authenticate(User $user) {
        $this->validate($this->request, [
            'email'     => 'required|email',
            'password'  => 'required'
        ]);
        // Find the user by email
        $user = User::where('email', $this->request->input('email'))->first();
        
        $check = $this->checkServer($this->request->input('email'),$this->request->input('password'));

        // return json_encode($check,true);
        if (json_decode($check)->login == "success") {    
            // Verify the password and generate the token
            if ($user) {
                $payload = [
                    'iss' => "lumen-jwt", // Issuer of the token
                    'sub' => $user->id, // Subject of the token
                    'iat' => time(), // Time when JWT was issued. 
                    'exp' => time() + 60*60 // Expiration time
                ];
                $user['payload'] = $payload;
                $jwt = JWT::encode($user,env('JWT_SECRET'));
                $response = [
                    "message" => "success",
                    "token" => $jwt,
                    "data" => $user,
                ];
                return response()->json($response,200);
            }else{
                $response = [
                    "result" => "failed",
                    "message" => "Email or Password does not exist on database",
                    // "Data User" => $user,
                ];
                return response()->json($response,401);
            }
        }else{
            $response = [
                "result" => "failed",
                "message" => "Email or Password is not in the carsworld database",
                // "Data User" => $user,
            ];
            return response()->json($response,401);
        }

        // Bad Request response
        return response()->json([
            'error' => 'Email or password is wrong.'
        ], 401);
    }
    
    public function register(Request $request){
        $register = new User();

	    $register->name = $request->name;
	    $register->email = $request->email;
        $register->password = Hash::make($request->password);
        $register->level = $request->level;

        $response['success'] = true;

        if ($register && $register->save()) {
            $response['message'] = 'Success';
            $response['data'] = $register;
        }else{
            $response['success'] = false;
            $response['message'] = 'Failed';
            $response['data'] = '';
        }
		return response()->json($response);
    }

    public function getUserJWT($id){
        $users = User::where('id',$id)->first();
        return response()->json($users);
    }
}