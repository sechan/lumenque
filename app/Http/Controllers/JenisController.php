<?php

namespace App\Http\Controllers;

use App\Models\Jenis;
use Illuminate\Http\Request;
use DB;

class JenisController extends Controller
{
    /**
     * Retrieve the user for the given ID.
     *
     * @param  int  $id
     * @return Response
     */
    public function index()
    {
        // getnama();
        $data = Jenis::get();
        return response()->json($data);
    }

    public function getData($id){
        $data = Jenis::where('id',$id)->first();
        return response()->json($data);
    }

    public function save(Request $request){
        $no = Jenis::select('id')->max('id');
        $id = sprintf("%03s", $no+1);
        $kode = 'SN'.$id;
        // return $kode;
        $data = Jenis::create([
            'nama' => $request->nama,
            'kode' => $kode
        ]);
        return response()->json($data);
    }

    public function update(Request $request){
        $data = Jenis::find($request->input('id'));
        // return $request->all();
        $data->nama = $request->input('nama');
        $data->kode = $request->input('kode');

        if($data && $data->save()){
            return response()->json($data);
        }else{
            return response()->json('gagal');
        }
    }

    public function delete($id){
        $data = Jenis::where('id',$id)->first();
        $data->delete();
        return response()->json('Delete Successfully',200);
    }
}