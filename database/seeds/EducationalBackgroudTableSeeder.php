<?php

use Illuminate\Database\Seeder;

class EducationalBackgroudTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\EducationalBackground::class, 5)->create();
    }
}
