<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('company_id');
            $table->integer('division_id');
            $table->integer('religion_id');
            $table->integer('country_id');
            $table->string('nip');
            $table->string('member_name');
            $table->enum('gender', ['male','female']);
            $table->string('email',191)->unique();
            $table->bigInteger('phone')->unique();
            $table->string('address');
            $table->date('date_birth');
            $table->date('date_join');
            $table->string('photo_profile');
            $table->tinyInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
